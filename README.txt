
/******************/
 Pretty Pagination
 /*****************/

 Author: Bill Wu
 Development Began 2012-7-7

This module changes default pagination layout. It allows user to easily navigate
around large number of pages. It is very useful when there are over 100 pages.
It is easy to change the design with css. You can replace the arrow with images.

Browser with user agent "Mozilla/5.0 (X11; Linux i686)" or even older version 
will have minor issue on overlay. Pagination is still working on those browsers
but the current page address will not be updated on address bar.

Sample CSS code:
.item-list ul.pager li.pager-first, .item-list ul.pager li.pager-previous, .item-list ul.pager li.pager-next, .item-list ul.pager li.pager-last {
  background-position:center;
  background-repeat:no-repeat;
  color: transparent;
  height:20px;
  width:20px;
}

.item-list ul.pager.prettypagination-class-processed li a {
  color: transparent;
}

.item-list ul.pager li.pager-input {
  margin-left:5px;
  margin-right:5px;
}

.item-list ul.pager li.pager-input input {
  border: 1px solid #A0A0A0;
  height:17px;
  margin-top:-2px;
  padding:2px;
  text-align:center;
  width:36px;
}

.item-list ul.pager li.pager-first.first.active {background-image:url('../images/arrow/arrow-firstpage-active.png');}
.item-list ul.pager li.pager-previous.active {background-image:url('../images/arrow/arrow-back-active.png');}
.item-list ul.pager li.pager-next.active {background-image:url('../images/arrow/arrow-next-active.png');}
.item-list ul.pager li.pager-last.active {background-image:url('../images/arrow/arrow-lastpage-active.png');}
.item-list ul.pager li.pager-first.first.inactive {background-image:url('../images/arrow/arrow-firstpage.png');}
.item-list ul.pager li.pager-previous.inactive {background-image:url('../images/arrow/arrow-back.png');}
.item-list ul.pager li.pager-next.inactive {background-image:url('../images/arrow/arrow-next.png');}
.item-list ul.pager li.pager-last.inactive {background-image:url('../images/arrow/arrow-lastpage.png');}
